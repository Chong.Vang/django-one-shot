from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_details,
    }
    return render(request, "todos/detail.html", context)


def todo_create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.isvalid():
            new_list = form.save()
            # new_list.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
